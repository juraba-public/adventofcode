const fs = require('fs');
const getFileInputAsArray = (filename) => {
    return fs.readFileSync(`${filename}.txt`, 'utf-8').replaceAll('\r', '').split('\n');
}

const findResult = (s) => {
    let result = 0;
    const regex = /\d/g;
    const matches = s.match(regex);
    if (matches) {
        result = parseInt(matches[0] + matches[matches.length - 1]);
    }
    return result;
}

const textToDigit = (t) => {
    switch (t) {
        case 'one':
            return 1;
        case 'two':
            return 2;
        case 'three':
            return 3;
        case 'four':
            return 4;
        case 'five':
            return 5;
        case 'six':
            return 6;
        case 'seven':
            return 7;
        case 'eight':
            return 8;
        case 'nine':
            return 9;
        default:
            return t;

    }
}

// Part one
const solvePartOne = (name) => {
    let result = 0;
    const inputAsArray = getFileInputAsArray('day01');
    inputAsArray.forEach((line, index) => {
        result += findResult(line);
    });
    return result;
}

// Part two
const solvePartTwo = (name) => {
    let result = 0;
    const inputAsArray = getFileInputAsArray('day01');
    inputAsArray.forEach((line, index) => {
        const firstTextDigit = line.match(/(\d|one|two|three|four|five|six|seven|eight|nine)/);
        const lastTextDigit = line.match(/.*(\d|one|two|three|four|five|six|seven|eight|nine).*/);
        result += parseInt(textToDigit(firstTextDigit[1]) + textToDigit(lastTextDigit[1]));
        console.log(`Line ${line} : ${textToDigit(firstTextDigit[1])} + ${textToDigit(lastTextDigit[1])} = ${parseInt(textToDigit(firstTextDigit[1]) + textToDigit(lastTextDigit[1]))}`)
        console.log(`Result ${result}`)
    });
    return result;
}


console.log(solvePartOne(day01));
console.log(solvePartTwo(day01));