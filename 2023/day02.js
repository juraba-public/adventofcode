const fs = require('fs');
const getFileInputAsArray = (filename) => {
    return fs.readFileSync(`${filename}.txt`, 'utf-8').replaceAll('\r', '').split('\n');
}

/**
 * Get index from line starting by Game {index}
 * @param line
 * @returns {number}
 */
const getIndexFromLine = (line) => {
    const regex = /(Game \d+)/;
    const matches = line.match(regex);
    if (matches) {
        return parseInt(matches[0].replace('Game ', ''));
    }
    return -1;
}

const isGameValid = (line) => {
    return line.split(';').map(set => isSetValid(set))
        .reduce((acc, curr) => acc && curr, true);
}

const isSetValid = (line) => {
    let blueValue = line.match(/(\d+) blue/g);
    blueValue = blueValue ? blueValue[0].replace(' blue', '') : 0;
    let greenValue = line.match(/(\d+) green/g);
    greenValue = greenValue ? greenValue[0].replace(' green', '') : 0;
    let redValue = line.match(/(\d+) red/g);
    redValue = redValue ? redValue[0].replace(' red', '') : 0;
    return blueValue <= 14 && greenValue <= 13 && redValue <= 12;
}

const getGamePower = (line) => {
    const gamePower = [0,0,0]; // Red, Green, Blue
    line.split(';').forEach(set => {
        console.log(`Set: ${set}`);
        const setPower = getSetPower(set);
        gamePower[0] = checkForLowerPower(0, gamePower, setPower) ? setPower[0] : gamePower[0];
        gamePower[1] = checkForLowerPower(1, gamePower, setPower) ? setPower[1] : gamePower[1];
        gamePower[2] = checkForLowerPower(2, gamePower, setPower) ? setPower[2] : gamePower[2];
    });
    console.log(`Game Power: ${gamePower}`);
    console.log(`Game Power: ${gamePower.reduce((acc, curr) => acc * parseInt(curr), 1)}`);
    return gamePower;
}

const checkForLowerPower = (index, gamePower, setPower) => gamePower[index] == 0 || (setPower[index] > 0 && gamePower[index] > setPower[index]);

const getSetPower = (line) => {
    let blueValue = line.match(/(\d+) blue/g);
    blueValue = blueValue ? blueValue[0].replace(' blue', '') : 0;
    let greenValue = line.match(/(\d+) green/g);
    greenValue = greenValue ? greenValue[0].replace(' green', '') : 0;
    let redValue = line.match(/(\d+) red/g);
    redValue = redValue ? redValue[0].replace(' red', '') : 0;
    return [redValue, greenValue, blueValue].map(value => parseInt(value));
}

// Part one
const solvePartOne = (name) => {
    let result = 0;
    const inputAsArray = getFileInputAsArray('day02');
    inputAsArray.forEach(line => {
        const index = getIndexFromLine(line);
        if (isGameValid(line.split(':')[1].trim())) {
            result += index;
        }
    });
    return result;
}

// Part two
const solvePartTwo = (name) => {
    let result = 0;
    const inputAsArray = getFileInputAsArray('day02');
    inputAsArray.forEach(line => {
        result += getGamePower(line.split(':')[1].trim()).reduce((acc, curr) => acc * parseInt(curr), 1);
    });
    return result;
}

// Execute code
console.log(`Part One: ${solvePartOne('day02')}`);
console.log(`Part Two: ${solvePartTwo('day02')}`);
