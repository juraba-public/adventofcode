const fs = require('fs');
const getFileInputAsArray = (filename) => {
    return fs.readFileSync(`${filename}.txt`, 'utf-8').replaceAll('\r', '').split('\n');
}

const calculateScore = (listToParse) => {
    let scoreMap = new Map();
    listToParse.forEach((line, index) => {
      let numb = parseInt(line)
        if (isNaN(numb)) {
            return;
        }
        if (scoreMap.has(numb)) {
            let current = scoreMap.get(numb)
            scoreMap.set(numb, current + 1)
        } else {
            scoreMap.set(numb, 1)
        }
    })
    return scoreMap;
}

const calculateSimilarity = (mapOne, mapTwo) => {
    let similarity = 0;
    mapOne.forEach((value, key) => {
        if (mapTwo.has(key)) {
            similarity += key * mapTwo.get(key)
            mapTwo.delete(key)
        }
    })
    return similarity;
}

const exo1 = () => {
    const inputAsArray = getFileInputAsArray('01');
    let leftInput = []
    let rightInput = []
    inputAsArray.forEach((line, index) => {
        line.split(' ').forEach((char, index) => {
            if (index % 2 === 0) {
                leftInput.push(char)
            } else {
                rightInput.push(char)
            }
        });
    });

    leftInput = leftInput.sort((a, b) => a - b)
    rightInput = rightInput.sort((a, b) => a - b)

    let result = 0;

    leftInput.forEach((char, index) => {
        if (isNaN(parseInt(char)) || isNaN(parseInt(rightInput[index]))) {
            return;
        }
        console.log("Add " + index)
        console.log("Pre : " + result)
        let sum= Math.abs(parseInt(char) - parseInt(rightInput[index]))
        console.log("Sum : " + sum)
        result += sum
        console.log("Post : " + result)
        console.log("==================")
    })
    return result;
}

const exo2 = () => {
    const inputAsArray = getFileInputAsArray('01');
    let leftInput = []
    let rightInput = []
    inputAsArray.forEach((line, index) => {
        line.split(' ').forEach((char, index) => {
            if (index % 2 === 0) {
                leftInput.push(char)
            } else {
                rightInput.push(char)
            }
        });
    });
    leftInput = leftInput.sort((a, b) => a - b)
    rightInput = rightInput.sort((a, b) => a - b)

    let leftMapScore = calculateScore(leftInput)
    let rightMapScore = calculateScore(rightInput)

    return calculateSimilarity(leftMapScore, rightMapScore)
}

console.log(exo1())
console.log(exo2())