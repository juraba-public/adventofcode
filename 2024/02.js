const fs = require('fs');
const getFileInputAsArray = (filename) => {
    return fs.readFileSync(`${filename}.txt`, 'utf-8').replaceAll('\r', '').split('\n');
}

const isReportSafe = (report) => {
    let isSafe = true;
    let reportVariance = 0;
    let index = 0;
    while (isSafe && index < report.length - 1) {
        let diff = report[index] - report[index + 1];
        if (Math.abs(diff) < 1 || Math.abs(diff) > 3) {
            isSafe = false;
        }
        if (index === 0) {
            reportVariance = Math.sign(diff);
        } else if (reportVariance !== Math.sign(diff)) {
            isSafe = false;
        }
        index++;
    }

    return isSafe;
}

const isReportSafeWithTolerance = (line, tolerance) => {
    const report = line.split(' ')
    let isSafe = isReportSafe(report);
    if (tolerance && !isSafe) {
        let index = 0;
        while (!isSafe && index < report.length) {
            if (index === 0) {
                isSafe = isReportSafe(report.slice(1, report.length));
            }
            else if (index === report.length - 1) {
                isSafe = isReportSafe(report.slice(0, report.length - 1));
            } else {
                isSafe = isReportSafe(report.slice(0, index).concat(report.slice(index + 1, report.length)));
            }
            index++;
        }
    }
    return isSafe;
}

const exo1 = () => {
    const inputAsArray = getFileInputAsArray('02');
    let safeReports = [];
    inputAsArray.forEach((line, index) => {
        if (isReportSafeWithTolerance(line, false)) {
            safeReports.push(line);
        }
    });
    return safeReports.length;
}

const exo2 = () => {
    const inputAsArray = getFileInputAsArray('02');
    let safeReports = [];
    inputAsArray.forEach((line, index) => {
        if (isReportSafeWithTolerance(line, true)) {
            safeReports.push(line);
        }
    });
    return safeReports.length;
}

console.log(exo1());
console.log(exo2());